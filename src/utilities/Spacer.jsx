import styled from "styled-components"
import { Box } from "grommet"
const Spacer = (height) => {
  return (
    <SpacerBox height={height}/>
  )
}

export default Spacer

const SpacerBox = styled(Box)`
  height: ${(props) => props.height};
`