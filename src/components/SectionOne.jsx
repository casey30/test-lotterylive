import React from "react";  
import logo from "../assets/the-lottery-live-logo.png";
import { Box, Image, Text, TextInput } from "grommet";
import styled from "styled-components";
import Spacer from "../utilities/Spacer";

 const SectionOne = ({isMobile}) => {
   console.log("hello")
   console.log("hello", isMobile)
  return (
    <SectionOneBox>
        <LogoBox>
          <LogoImage isMobile={isMobile} src={logo} />
        </LogoBox>
        <Spacer height={"40px"} />
        <PlayScratchersBox>
          <PlayScratchersText isMobile={isMobile}>
            Play Scratcher Games Live. <br/>
            Coming "Month" 2022.
          </PlayScratchersText>
        </PlayScratchersBox>
        <Spacer height={"40px"} />
        <EmailBox>
          <EmailInputTextBox isMobile={isMobile}>
            <InputBox>
              <EmailTextInput isMobile={isMobile} placeholder={"Your email..."}></EmailTextInput>
            </InputBox>
            <InputMessageBox>
              <InputMessageText>Keep me up to date</InputMessageText>
            </InputMessageBox>
          </EmailInputTextBox>
        </EmailBox>
      </SectionOneBox>
  )
}
export default SectionOne

const SectionOneBox = styled(Box)`
  height: 80vh;
  justify-content: center;
  text-align: center;
  z-index: 99;
  margin-bottom: 10vh;
`;
const LogoImage = styled(Image)`
  max-width: ${(props) => (props.isMobile ? "300px" : "510px")};
`;
const LogoBox = styled(Box)`
  margin-top: -100px;
  justify-content: center;
  flex-direction: row;
`;
const PlayScratchersBox = styled(Box)``;
const PlayScratchersText = styled(Text)`
  font-family: Proxima Nova;
  font-size: ${(props) => (props.isMobile ? "16px" : "26px")};
  font-weight: 300;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.31;
  letter-spacing: 0.26px;
  text-align: center;
  color: #fff;
`;

const EmailBox = styled(Box)`
  justify-content: center;
  flex-direction: row;
`;
const EmailInputTextBox = styled(Box)`
  flex-direction: row;
  width: ${(props) => (props.isMobile ? "300px" : "600px")};
  background: rgb(155, 29, 23, 0.35);
  padding: 4px;
  border-color: solid 1px #f52f25;
  border-radius: 8px;
`;
const EmailTextInput = styled(TextInput)`
  border: solid 2px #f52f25;
  border-radius: 8px;
  height: ${(props) => (props.isMobile ? "20px" : "50px")};
  color: white;
`;
const InputBox = styled(Box)`
  font-family: Proxima Nova;
  border-radius: 8px;
  font-size: ${(props) => (props.isMobile ? "10px" : "18px")};
  width: 60%;
  margin: 5px;
  
  background-color: #98241d;
`;
const InputMessageBox = styled(Box)`
  margin: 5px;
  background-color: #c73a31;
  width: 50%;
  
  border-radius: 8px;
  justify-content: center;
`;

const InputMessageText = styled(Text)`
  font-family: Proxima Nova;
  font-size: ${(props) => (props.isMobile ? "12px" : "18px")};
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: 0.2px;
  text-align: center;
  color: #fafafa;
`;

