import React, { Component } from 'react';
import Lottie from 'react-lottie';
import animationData from '../assets/animations/LottoTicketsAnimation.json';
import getWindowDimensions from '../hooks/getWindowDimensions'

const UncontrolledLottie = () => {
  const {width} = getWindowDimensions()
  const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: animationData,
      rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice',
      }
    }
    return (
      <div>
        <Lottie
          options={defaultOptions}
          width={width}
        />
      </div>
    )
}

export default UncontrolledLottie;

//If you have animations on After Effects that you would like to use, you can export them to JSON using the Bodymovin plugin for After Effects.