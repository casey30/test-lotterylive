import React from "react";  
import oneInFourChangeFeature from "../assets/one-four-chance-feature.png";
import purchaseScratcherTicketsFeature from "../assets/purchase-scratcher-tickets-feature.png";
import ticketsScratchedOffFeature from "../assets/tickets-scratched-off-feature.png";
import { Box, Image, Text } from "grommet";
import styled from "styled-components";
import Spacer from "../utilities/Spacer";

 const SectionThree = () => {
   
  return (
    <SectionThreeBox>
        <ContentBox>
          <div>
            <FeatureImage src={purchaseScratcherTicketsFeature} />
          </div>
          <div>
            <FeatureImage src={oneInFourChangeFeature} />
          </div>
          <div>
            <FeatureImage src={ticketsScratchedOffFeature} />
          </div>
        </ContentBox>
      </SectionThreeBox>
  )
}
export default SectionThree

const SectionThreeBox = styled(Box)`
  width: 100%;
  height: 50vh;

  justify-content: center;
  z-index: 99;
`;

const FeatureImage = styled(Image)`
  max-width: ${(props) => (props.isMobile ? "125px" : "400px")};
`;
const ContentBox = styled(Box)`
  width: 100%;
  justify-content: center;
  flex-direction: row;
`;