import React from "react";  
import presenter from "../assets/presenter.png";
import profileIcon from "../assets/profile-icon.png";
import twitterIcon from "../assets/twitter-icon.png";
import instagramIcon from "../assets/instagram-icon.png";
import cogIcon from "../assets/cog-icon.png";
import { Box, Image, Text } from "grommet";
import styled from "styled-components";
import Spacer from "../utilities/Spacer";
import getWindowDimensions from '../hooks/getWindowDimensions'

 const SectionTwo = () => {
    const {width} = getWindowDimensions()
    let isMobile = width < 980
  return (
    <SectionTwoBox isMobile={isMobile}>
        <ContentBox>
          <PresenterBox>
            <PresenterImage src={presenter} isMobile={isMobile}/>
          </PresenterBox>
          <InfoBox>
            <Spacer height={"20%"} />
            <HeadlineBox>
              <IconBox isMobile={isMobile}>
                <ProfileIconImage src={profileIcon} />
              </IconBox>
              <IntroducingText isMobile={isMobile}>
                Introducing our <br/>Lottery Live Host.
              </IntroducingText>
            </HeadlineBox>
            <Spacer height={"10%"} />
            <DescriptionBox isMobile={isMobile}>
              <SeparationLine />
              <Spacer height={isMobile ? '10px' : '30px'} />
              <DescriptionText isMobile={isMobile}>
                <NameText isMobile={isMobile}>Full Name</NameText> sit amet, consectetur adipiscing
                elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                aliqua. Ut enimio adsi minim veniam, quisito nostrudoa
                exercitation ullamco laboris nisi uto aliquip ex ea commodo
                consequat. 
              </DescriptionText>
              <Spacer height={isMobile ? '10px' : '30px'} />
              <SeparationLine />
              <Spacer height={"10px"} />
              <SocialMediaBoxContainer>
                <SocialMediaBox>
                <NameTextContainer isMobile={isMobile}>
                  <NameText isMobile={isMobile}>Full Name</NameText>
                </NameTextContainer>
                
                <SocialIconBox isMobile={isMobile}>
                  <SocialMediaIconImage isMobile={isMobile} src={twitterIcon} />
                </SocialIconBox>
                <SocialIconBox isMobile={isMobile}>
                  <SocialMediaIconImage isMobile={isMobile} src={instagramIcon} />
                </SocialIconBox>
              </SocialMediaBox>
              </SocialMediaBoxContainer>
              
            </DescriptionBox>
            <Spacer height={"10%"} />
            <HeadlineBox>
              <IconBox isMobile={isMobile}>
                <ProfileIconImage src={cogIcon} />
              </IconBox>
              <HowWillText isMobile={isMobile}>How will Lottery <br/>Live Work?</HowWillText>
            </HeadlineBox>
          </InfoBox>
        </ContentBox>
      </SectionTwoBox>
  )
}
export default SectionTwo

const SectionTwoBox = styled(Box)`
  width: 100%;
  height: 90vh;
  margin-bottom: ${(props) => (props.isMobile ? "0vh" : "70vh")};
  justify-content: center;
  z-index: 99;
`;
const ContentBox = styled(Box)`
  width: 100%;
  flex-direction: row;
`;
const PresenterBox = styled(Box)`
  width: 60%;
`;
const PresenterImage = styled(Image)`
  max-width: ${(props) => (props.isMobile ? "700px" : "1200px")};
  width: 100%;
`;
const InfoBox = styled(Box)`
  padding: 0px 10px;
  width: 45%;
  margin-left: ${(props) => (props.isMobile ? "0px" : "20px")};
`;
const ProfileIconImage = styled(Image)`
  max-width: 20px;
  tint-color: #fff;
`;
const HeadlineBox = styled(Box)`
  flex-direction: row;
  margin: ${(props) => (props.isMobile ? "0px 0px 0px 0px;" : "50px 0px 20px 0px;")};
`;
const IntroducingText = styled(Text)`
  font-family: Proxima Nova;
  font-size: ${(props) => (props.isMobile ? "14px" : "30px")};
  line-height: 1.07;
  font-weight: bold;
  letter-spacing: 0.2px;
  text-align: left;
  color: #fff;
`;
const DescriptionBox = styled(Box)`
  max-width: ${(props) => (props.isMobile ? "100%" : "90%")};
`;
const NameText = styled(Text)`
  font-size: ${(props) => (props.isMobile ? "8px" : "14px")};
  font-family: Proxima Nova;
  margin: 0px;
  font-style: bold;
  color: #fff;
  line-height: 1.43;
  letter-spacing: 0.07px;
`;
const SeparationLine = styled(Box)`
  width: 100%;
  border: solid 0.5px #f52f25;
`;
const DescriptionText = styled(Text)`
  font-family: Proxima Nova;
  font-size: ${(props) => (props.isMobile ? "7px" : "14px")};
  font-weight: 500;
  line-height: 1.43;
  letter-spacing: 0.07px;
  text-align: left;
  color: #f89c9c;
  margin-right: 10%;
`;
const HowWillText = styled(Text)`
  font-family: Proxima Nova;
  font-size: ${(props) => (props.isMobile ? "14px" : "30px")};
  line-height: 1.07;
  font-weight: bold;
  letter-spacing: 0.2px;
  text-align: left;
  color: #fff;
`;
const IconBox = styled(Box)`
  margin-top: ${(props) => (props.isMobile ? "0px" : "6px")};
  padding: ${(props) => (props.isMobile ? "0px 5px 0px 0px;" : "0px 15px 0px 0px;")};
`;
const SocialMediaBoxContainer = styled(Box)`
  margin-top: 8px;
  margin-right: 10%`
const SocialMediaBox = styled(Box)`
  flex-direction: row;
  justify-content: flex-end;
  margin-top: 5px;
`;

const SocialMediaIconImage = styled(Image)`
  max-width: 8px;
`;
const SocialIconBox = styled(Box)`
  background-color: #c73a31;
  min-width: 22px;
  min-height: 22px;
  padding: ${(props) => (props.isMobile ? "3px" : "8px")};
  border-radius: 30px;
  margin: ${(props) => (props.isMobile ? "0px" : "2px")};
`;
const NameTextContainer = styled(Box)`
  margin: ${(props) => (props.isMobile ? "0px 0px 0px 0px;" : "5px 8px 0px 0px;")};
`;