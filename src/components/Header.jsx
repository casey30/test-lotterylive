import React from "react";  
import logo from "../assets/logolotteryball.png";
import { Box, Image, Text } from "grommet";
import styled from "styled-components";
import getWindowDimensions from '../hooks/getWindowDimensions'

 const Header = () => {
   const {width} = getWindowDimensions()
   let isMobile = width < 980
  return (
    <StyledHeader width={width.toString()}>
        <ImageBox height="small" width="small">
          <Image src={logo} height={isMobile ? '43px' : '90px'} width={isMobile ? '43px' : '90px'} isMobile={isMobile}/>
        </ImageBox>
        <RightBox isMobile={isMobile}>
          <CreateAccountText isMobile={isMobile}>Create An Account</CreateAccountText>
        </RightBox>
      </StyledHeader>
  )
}
export default Header


const StyledHeader = styled(Box)`
  height: 140px;
  background-color: transparent;
  justify-content: space-between;
  flex-direction: row;
  width: ${(props) => props.width}px;
  padding: 0px 40px;
  z-index: 99;
`;
const ImageBox = styled(Box)`
  margin-top: 30px;
`;

const RightBox = styled(Box)`
  margin-right: ${(props) => props.isMobile ? '140px' : '30px'};
`
const CreateAccountText = styled(Text)`
  width: 172px;
  height: 12px;
  margin: 0 0 157px 80px;
  font-family: Proxima Nova;
  font-size: ${(props) => props.isMobile ? '14px' : '16px'};
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 6.25;
  letter-spacing: 0.08px;
  text-align: right;
  color: #fff;
`