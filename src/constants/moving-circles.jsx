import styled from "styled-components";
import { Box } from "grommet";

export const movingCircle = styled(Box) `
  display: block;
    width: 800px;
    height: 800px;
    border-radius: 50%;
    background-color: #999999;
    box-shadow: inset -5px -5px 5px rgba(0, 0, 0, 0.6),
      15px 15px 2px rgba(0, 0, 0, 0.04);
    position: absolute;
    -webkit-animation: moveX 24s linear 0s infinite alternate,
      moveY 24.4s linear 0s infinite alternate;
    -moz-animation: moveX 24s linear 0s infinite alternate,
      moveY 24.4s linear 0s infinite alternate;
    -o-animation: moveX 24s linear 0s infinite alternate,
      moveY 24.4s linear 0s infinite alternate;
    animation: moveX 24s linear 0s infinite alternate,
      moveY 24.4s linear 0s infinite alternate;
  }

  @-webkit-keyframes moveX {
    from {
      left: 0;
    }
    to {
      left: ${(props) => props.width};
    }
  }
  @-moz-keyframes moveX {
    from {
      left: 0;
    }
    to {
      left: ${(props) => props.width};
    }
  }
  @-o-keyframes moveX {
    from {
      left: 0;
    }
    to {
      left: ${(props) => props.width};
    }
  }
  @keyframes moveX {
    from {
      left: 0;
    }
    to {
      left: ${(props) => props.width};
    }
  }

  @-webkit-keyframes moveY {
    from {
      top: 0;
    }
    to {
      top: ${(props) => props.height};
    }
  }
  @-moz-keyframes moveY {
    from {
      top: 0;
    }
    to {
      top: ${(props) => props.height};
    }
  }
  @-o-keyframes moveY {
    from {
      top: 0;
    }
    to {
      top: ${(props) => props.height};
    }
  }
  @keyframes moveY {
    from {
      top: 0;
    }
    to {
      top: ${(props) => props.height};
    }
`