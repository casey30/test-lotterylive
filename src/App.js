import React, { useEffect, useState } from "react";

import "./App.css";

import { Box, Image, Text, TextInput } from "grommet";

import scratchOffs from "./assets/scratch-offs.png";
import liveText from "./assets/live-text.png";
import Header from "./components/Header";
import SectionOne from "./components/SectionOne";
import SectionTwo from "./components/SectionTwo";
import SectionThree from "./components/SectionThree";
import Spacer from "./utilities/Spacer";
import styled from "styled-components";

import getWindowDimensions from "./hooks/getWindowDimensions";
import UncontrolledLottie from "./components/UncontrolledLottie";

function App() {
  const { width } = getWindowDimensions();
  let isMobile = width < 980;
  console.log(isMobile);
  return (
    <StyledApp>
      <Bubble1 isMobile={isMobile} />
      <Bubble2 isMobile={isMobile} />
      <Bubble3 isMobile={isMobile} />
      <Bubble4 isMobile={isMobile} />
      <Bubble5 />
      <Bubble6 />

      <Bubble7 />
      <Bubble8 />
      <Bubble9 />
      <Bubble10 />
      <Bubble11 />
      <Bubble12 />
      <Bubble13 />
      <Bubble14 />
      <Bubble15 />
      <Bubble16 />
      <Header />
      <SectionOne isMobile={isMobile} />
      <SectionTwo />
      <SectionThree />
      <SectionFour>
        {/* <ScratchOffsImage src={scratchOffs} /> */}
        <UncontrolledLottie />
      </SectionFour>
      <SectionFive>
        <LongBoxContainer>
          <LongBoxContent>
            <TheLotteryLiveBoxContainer>
              <TheLotteryLiveBox>
                <TheLotteryText>The Lottery.</TheLotteryText>
                <LiveImage src={liveText} />
              </TheLotteryLiveBox>
            </TheLotteryLiveBoxContainer>

            <CreateAnAccountBox>
              <CreateAccountText>Create an Account</CreateAccountText>
            </CreateAnAccountBox>
          </LongBoxContent>
        </LongBoxContainer>
      </SectionFive>
    </StyledApp>
  );
}

export default App;

const Bubble1 = styled(Box)`
  position: absolute;
  margin-left: ${(props) => (props.isMobile ? "400px" : "400px")};
  margin-top: ${(props) => (props.isMobile ? "-150px" : "-300px")};
  width: ${(props) => (props.isMobile ? "400px" : "900px")};
  height: ${(props) => (props.isMobile ? "400px" : "900px")};
  opacity: 0.25;
  border: solid 1 #151515;
  background-color: #982118;
  border-radius: 460px;
`;

const Bubble2 = styled(Box)`
  position: absolute;
  margin-left: ${(props) => (props.isMobile ? "400px" : "50px")};
  margin-top: ${(props) => (props.isMobile ? "-150px" : "-50px")};
  width: ${(props) => (props.isMobile ? "400px" : "900px")};
  height: ${(props) => (props.isMobile ? "400px" : "900px")};
  opacity: 0.25;
  border: solid 0 #151515;
  background-color: #982118;
  border-radius: 460px;
  z-index: 0;
`;

const Bubble3 = styled(Box)`
  position: absolute;
  margin-left: ${(props) => (props.isMobile ? "400px" : "650px")};
  margin-top: ${(props) => (props.isMobile ? "-150px" : "-350px")};
  width: ${(props) => (props.isMobile ? "400px" : "900px")};
  height: ${(props) => (props.isMobile ? "400px" : "900px")};
  opacity: 0.25;
  border: solid 0 #151515;
  background-color: #982118;
  border-radius: 460px;
  z-index: 0;
`;

const Bubble4 = styled(Box)`
  position: absolute;
  margin-left: ${(props) => (props.isMobile ? "-100px" : "-100px")};
  margin-top: ${(props) => (props.isMobile ? "-150px" : "-150px")};
  width: ${(props) => (props.isMobile ? "400px" : "900px")};
  height: ${(props) => (props.isMobile ? "400px" : "900px")};
  opacity: 0.25;
  border: solid 0 #151515;
  background-color: #982118;
  border-radius: 460px;
  z-index: 0;
`;

const Bubble5 = styled(Box)`
  position: absolute;
  margin-left: ${(props) => (props.isMobile ? "-100px" : "1000px")};
  margin-top: ${(props) => (props.isMobile ? "-150px" : "450px")};
  width: ${(props) => (props.isMobile ? "400px" : "900px")};
  height: ${(props) => (props.isMobile ? "400px" : "900px")};
  opacity: 0.25;
  border: solid 0 #151515;
  background-color: #982118;
  border-radius: 460px;
  z-index: 0;
`;

const Bubble6 = styled(Box)`
  position: absolute;
  margin-left: ${(props) => (props.isMobile ? "-100px" : "1000px")};
  margin-top: ${(props) => (props.isMobile ? "-150px" : "650px")};
  width: ${(props) => (props.isMobile ? "400px" : "900px")};
  height: ${(props) => (props.isMobile ? "400px" : "900px")};
  opacity: 0.25;
  border: solid 0 #151515;
  background-color: #982118;
  border-radius: 460px;
  z-index: 0;
`;

const Bubble7 = styled(Box)`
  position: absolute;
  margin-left: ${(props) => (props.isMobile ? "-100px" : "200px")};
  margin-top: ${(props) => (props.isMobile ? "-150px" : "650px")};
  width: ${(props) => (props.isMobile ? "400px" : "900px")};
  height: ${(props) => (props.isMobile ? "400px" : "900px")};
  opacity: 0.25;
  border: solid 0 #151515;
  background-color: #982118;
  border-radius: 460px;
  z-index: 0;
`;

const Bubble8 = styled(Box)`
  position: absolute;
  margin-left: ${(props) => (props.isMobile ? "-100px" : "500px")};
  margin-top: ${(props) => (props.isMobile ? "-150px" : "650px")};
  width: ${(props) => (props.isMobile ? "400px" : "900px")};
  height: ${(props) => (props.isMobile ? "400px" : "900px")};
  opacity: 0.25;
  border: solid 0 #151515;
  background-color: #982118;
  border-radius: 460px;
  z-index: 0;
`;

const Bubble9 = styled(Box)`
  position: absolute;
  margin-left: ${(props) => (props.isMobile ? "-100px" : "200px")};
  margin-top: ${(props) => (props.isMobile ? "-150px" : "1650px")};
  width: ${(props) => (props.isMobile ? "400px" : "900px")};
  height: ${(props) => (props.isMobile ? "400px" : "900px")};
  opacity: 0.25;
  border: solid 0 #151515;
  background-color: #982118;
  border-radius: 460px;
  z-index: 0;
`;

const Bubble10 = styled(Box)`
  position: absolute;
  margin-left: ${(props) => (props.isMobile ? "-100px" : "700px")};
  margin-top: ${(props) => (props.isMobile ? "-150px" : "1350px")};
  width: ${(props) => (props.isMobile ? "400px" : "900px")};
  height: ${(props) => (props.isMobile ? "400px" : "900px")};
  opacity: 0.25;
  border: solid 0 #151515;
  background-color: #982118;
  border-radius: 460px;
  z-index: 0;
`;

const Bubble11 = styled(Box)`
  position: absolute;
  margin-left: ${(props) => (props.isMobile ? "-100px" : "-100px")};
  margin-top: ${(props) => (props.isMobile ? "-150px" : "1450px")};
  width: ${(props) => (props.isMobile ? "400px" : "900px")};
  height: ${(props) => (props.isMobile ? "400px" : "900px")};
  opacity: 0.25;
  border: solid 0 #151515;
  background-color: #982118;
  border-radius: 460px;
  z-index: 0;
`;

const Bubble12 = styled(Box)`
  position: absolute;
  margin-left: ${(props) => (props.isMobile ? "-100px" : "700px")};
  margin-top: ${(props) => (props.isMobile ? "-150px" : "1650px")};
  width: ${(props) => (props.isMobile ? "400px" : "900px")};
  height: ${(props) => (props.isMobile ? "400px" : "900px")};
  opacity: 0.25;
  border: solid 0 #151515;
  background-color: #982118;
  border-radius: 460px;
  z-index: 0;
`;

const Bubble13 = styled(Box)`
  position: absolute;
  margin-top: -150px;
  margin-left: 160px;
  width: 400px;
  height: 400px;
  opacity: 0.2;
  border: solid 0 #151515;
  background-color: #982118;
  border-radius: 360px;
  z-index: 0;
`;

const Bubble14 = styled(Box)`
  position: absolute;
  display: inline-block;
  margin-top: 380px;
  margin-left: -20px;
  width: 400px;
  height: 400px;
  opacity: 0.2;
  border: solid 0 #151515;
  background-color: #982118;
  border-radius: 360px;
  z-index: 0;
`;

const Bubble15 = styled(Box)`
  position: absolute;
  display: inline-block;
  margin-top: 1100px;
  margin-left: 80px;
  width: 400px;
  height: 400px;
  opacity: 0.2;
  border: solid 0 #151515;
  background-color: #982118;
  border-radius: 360px;
  z-index: 0;
`;

const Bubble16 = styled(Box)`
  position: absolute;
  display: inline-block;
  margin-top: 1300px;
  margin-left: 150px;
  width: 400px;
  height: 400px;
  opacity: 0.2;
  border: solid 0 #151515;
  background-color: #982118;
  border-radius: 360px;
  z-index: 0;
`;

const StyledApp = styled(Box)`
  max-width: 100vw;
  max-height: 420vh;
  overflow-x: hidden;
  overflow-y: hidden;
  background-color: #db1109;
`;
const SectionFour = styled(Box)`
  max-height: 40vh;
  overflow: hidden;
  z-index: 99;
`;
const ScratchOffsImage = styled(Image)``;

const SectionFive = styled(Box)`
  width: 100%;
  height: 70vh;
  justify-content: center;

  overflow-x: hidden;
  overflow-y: hidden;
  z-index: 99;
`;
const LongBoxContent = styled(Box)`
  height: ${(props) => (props.isMobile ? "auto" : "200px")};
  width: 90%;
  flex-direction: row;
  justify-content: space-between;
  background-color: #b52820;
  padding: ${(props) => (props.isMobile ? "20px" : "0px 100px 0px 100px")};
  border-radius: 400px;
  box-shadow: 0px 12px 15.1px 1.9px rgba(0, 0, 0, 0.45);
  padding-top: ${(props) => (props.isMobile ? "35px" : "0px")};
`;
const LongBoxContentContainer = styled(Box)`
  width: 100%;
  justify-content: center;
`;
const LongBoxContainer = styled(Box)`
  width: 100%;
  justify-content: center;
  flex-direction: row;
`;
const TheLotteryLiveBoxContainer = styled(Box)`
  justify-content: center;
`;
const TheLotteryLiveBox = styled(Box)`
  flex-direction: row;
`;
const LiveImage = styled(Image)`
  margin-left: -2px;
  margin-top: ${(props) => (props.isMobile ? "0px" : "-10px")};
  max-width: ${(props) => (props.isMobile ? "60px" : "130px")};
`;
const CreateAnAccountBox = styled(Box)`
  justify-content: center;
`;
const TheLotteryText = styled(Text)`
  font-family: Proxima Nova;
  font-size: ${(props) => (props.isMobile ? "22px" : "52px")};
  font-weight: 700;
  letter-spacing: 0px;
  color: #fff;
`;
const CreateAccountText = styled(Text)`
  font-family: Proxima Nova;
  font-size: ${(props) => (props.isMobile ? "8px" : "16px")};
  letter-spacing: 0.58px;
  color: #fff;
`;
